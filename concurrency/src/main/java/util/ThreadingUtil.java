package util;

public class ThreadingUtil {

    public static void runParallel(int threadCount, Runnable runnable) {
        for (int i = 0; i < threadCount; i++) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            runParallel(runnable);
        }
    }

    public static void runParallel(Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.start();
    }

}
