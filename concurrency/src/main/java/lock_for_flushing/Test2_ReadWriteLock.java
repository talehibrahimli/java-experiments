package lock_for_flushing;

import util.ThreadingUtil;

import java.util.concurrent.locks.*;

// Blocking threads from different method
// Working!
public class Test2_ReadWriteLock {

    public static void main(String... args) {
        Test2_ReadWriteLock test1 = new Test2_ReadWriteLock();
        ThreadingUtil.runParallel(test1::start);
        ThreadingUtil.runParallel(5000, test1::run);
    }

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    private final Lock readLock = readWriteLock.readLock();

    private final Lock writeLock = readWriteLock.writeLock();

    private void start() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("TRY BLOCKING");
        writeLock.lock();
        System.out.println("START BLOCKING");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("END BLOCKING");
        writeLock.unlock();
    }

    public void run() {
        readLock.lock();
        try {
            System.out.println("incoming thread: " + Thread.currentThread().getName());
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("outgoing thread: " + Thread.currentThread().getName());
        readLock.unlock();
    }

}
