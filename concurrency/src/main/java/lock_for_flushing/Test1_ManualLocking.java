package lock_for_flushing;

import util.ThreadingUtil;

import java.util.concurrent.atomic.AtomicInteger;

// Blocking threads from different method
// Working!
public class Test1_ManualLocking {

    public static void main(String... args) {
        Test1_ManualLocking test1 = new Test1_ManualLocking();
        ThreadingUtil.runParallel(test1::flush);
        ThreadingUtil.runParallel(5000, test1::run);
    }

    private volatile boolean locked = false;
    private volatile AtomicInteger flushLocked = new AtomicInteger();

    private void flush() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        locked = true;
        System.out.println("TRY BLOCKING");
        while (flushLocked.get() != 0) {
            Thread.yield();
        }
        System.out.println("START BLOCKING");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("END BLOCKING");
        locked = false;
    }

    public void run() {
        try {
            while (locked) {
                Thread.yield();
            }
            flushLocked.addAndGet(1);
            System.out.println("incoming thread: " + Thread.currentThread().getName());
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("outgoing thread: " + Thread.currentThread().getName());
        flushLocked.addAndGet(-1);
    }

}
